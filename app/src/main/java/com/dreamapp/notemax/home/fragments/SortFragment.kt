package com.dreamapp.notemax.home.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import com.dreamapp.notemax.R
import com.dreamapp.notemax.core.isDarkThemeOn
import com.dreamapp.notemax.databinding.FragmentSortBinding
import com.dreamapp.notemax.domain.viewmodel.NotesViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.transition.platform.MaterialFadeThrough

class SortFragment : BottomSheetDialogFragment() {

    private var _binding: FragmentSortBinding? = null
    private val binding get() = _binding!!

    private val notesViewModel: NotesViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enterTransition = MaterialFadeThrough()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = initViewBinding(inflater)

    private fun initViewBinding(inflater: LayoutInflater): View {
        _binding = FragmentSortBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //validateCustomTheme()
        binding.alphabetSort.setOnClickListener {
            notesViewModel.orderSelected.value = 1
            dismiss()
        }
        binding.dateUpdateSort.setOnClickListener {
            notesViewModel.orderSelected.value = 2
            dismiss()
        }
        binding.colorSort.setOnClickListener {
            notesViewModel.orderSelected.value = 3
            dismiss()
        }
    }

    private fun validateCustomTheme() {
        if (requireContext().isDarkThemeOn())
            binding.sortContainer.background.setTint(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.colorBlack
                )
            )
        else binding.sortContainer.background.setTint(
            ContextCompat.getColor(
                requireContext(),
                R.color.colorWhite
            )
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}