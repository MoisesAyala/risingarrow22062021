package com.dreamapp.notemax.home.fragments

import android.animation.LayoutTransition
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.dreamapp.notemax.R
import com.dreamapp.notemax.core.isDarkThemeOn
import com.dreamapp.notemax.core.OnNoteClickListener
import com.dreamapp.notemax.core.toggleTop
import com.dreamapp.notemax.databinding.FragmentNotesBinding
import com.dreamapp.notemax.domain.model.Note
import com.dreamapp.notemax.domain.viewmodel.NotesViewModel
import com.dreamapp.notemax.home.HomeActivity
import com.dreamapp.notemax.home.adapters.NotesListAdapter
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class NotesFragment : Fragment() {

    private lateinit var activity: HomeActivity
    private lateinit var noteListAdapter: NotesListAdapter

    private var _binding: FragmentNotesBinding? = null
    private val binding get() = _binding!!

    private val notesViewModel: NotesViewModel by activityViewModels()

    private var firstSearch = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = context as HomeActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View = initViewBinding(inflater)

    private fun initViewBinding(inflater: LayoutInflater): View {
        _binding = FragmentNotesBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //validateCustomTheme()
        runActionListeners()
        noteListAdapter = NotesListAdapter(requireActivity())

        lifecycleScope.launch {
            notesViewModel.getNotes().collect { notes ->
                if (notes.isEmpty())
                    showEmptyNotes()
                else {
                    hideEmptyNotes()
                    setupAdapter()
                    notesViewModel.orderSelected.value?.let {
                        setSortSelectedData(it)
                    } ?: setupNoteList()
                }
            }
        }

        notesViewModel.orderSelected.observe(viewLifecycleOwner) {
            setSortSelectedData(it)
        }

        binding.viewType.setOnClickListener {
            if (binding.notesRecycler.layoutManager is LinearLayoutManager) {
                binding.viewType.icon =
                    ContextCompat.getDrawable(requireContext(), R.drawable.ic_view_list)
                binding.notesRecycler.layoutManager =
                    StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
            } else {
                binding.viewType.icon =
                    ContextCompat.getDrawable(requireContext(), R.drawable.ic_grid_view)
                binding.notesRecycler.layoutManager = LinearLayoutManager(requireContext())
            }
        }

        binding.searcherTop.search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return if (!newText.isNullOrEmpty()) {
                    notesViewModel.currentSearch = "%$newText%"
                    if (firstSearch) {
                        firstSearch = false
                        setupNoteSearchList()
                    } else noteListAdapter.refresh()
                    true
                } else {
                    firstSearch = true
                    setupNoteList()
                    false
                }
            }
        })
    }

    private fun setSortSelectedData(it: Int) {
        when (it) {
            1 -> submitOrderByTitle()
            2 -> submitOrderByDate()
            else -> submitOrderByColor()
        }
        lifecycleScope.launch {
            delay(600L)
            binding.notesRecycler.smoothScrollToPosition(0)
        }
        noteListAdapter.refresh()
    }

    private fun submitOrderByTitle() {
        lifecycleScope.launch {
            notesViewModel.titleOrderPaging.collectLatest { pagedData ->
                noteListAdapter.submitData(pagedData)
            }
        }
        binding.sorter.text = getString(R.string.order_by_title)
    }

    private fun submitOrderByDate() {
        lifecycleScope.launch {
            notesViewModel.dateOrderPaging.collectLatest { pagedData ->
                noteListAdapter.submitData(pagedData)
            }
        }
        binding.sorter.text = getString(R.string.order_by_date)
    }

    private fun submitOrderByColor() {
        lifecycleScope.launch {
            notesViewModel.colorOrderPaging.collectLatest { pagedData ->
                noteListAdapter.submitData(pagedData)
            }
        }
        binding.sorter.text = getString(R.string.order_by_color)
    }

    private fun setupNoteList() {
        lifecycleScope.launch {
            notesViewModel.notesPaging.collect { pagedData ->
                noteListAdapter.submitData(pagedData)
            }
        }
        noteListAdapter.refresh()
    }

    private fun setupNoteSearchList() {
        lifecycleScope.launch {
            notesViewModel.notesSearchPaging.collectLatest { pagedData ->
                noteListAdapter.submitData(pagedData)
            }
        }
    }

    private fun validateCustomTheme() {
        val transition = LayoutTransition()
        transition.disableTransitionType(LayoutTransition.DISAPPEARING)
        binding.searcherTop.searcherContainer.layoutTransition = transition
        if (requireContext().isDarkThemeOn())
            binding.containerMain.setBackgroundColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.colorBlack
                )
            )
        else binding.containerMain.setBackgroundColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.colorAlmostWhite
            )
        )
    }

    private fun showEmptyNotes() {
        binding.notesRecycler.isVisible = NOT_VISIBLE
        binding.emptyNotesView.root.isVisible = VISIBLE
        binding.btnSearch.isVisible = NOT_VISIBLE
        binding.options.isEnabled = false
        binding.viewType.isEnabled = false
    }

    private fun hideEmptyNotes() {
        binding.notesRecycler.isVisible = VISIBLE
        binding.emptyNotesView.root.isVisible = NOT_VISIBLE
        binding.btnSearch.isVisible = VISIBLE
        binding.options.isEnabled = true
        binding.viewType.isEnabled = true
    }

    private fun runActionListeners() {
        binding.addNote.setOnClickListener {
            activity.goToNoteEditor(it)
        }

        binding.options.setOnClickListener {
            goToFilters()
        }
        binding.login.setOnClickListener {
            //println(compress("Durante las operaciones de copia de seguridad automática y restablecimiento, el sistema inicia la app en modo restringido para evitar que acceda a archivos que podrían causar conflictos y permitir que ejecute métodos de devolución de llamada en su BackupAgent. En este modo restringido, la actividad principal de la app no se inicia automáticamente, sus proveedores de contenido no se inicializan y se crea una instancia para la clase base Application en lugar de cualquier subclase declarada en el manifiesto de la app."))
            //val loginFragment = LoginFragment()
            //loginFragment.show(parentFragmentManager, "")
        }
        binding.btnSearch.setOnClickListener {
            showSearcher()
        }
    }

    private fun showSearcher() {
        binding.searcherTop.searcherContainer.toggleTop()
        binding.searcherTop.btnHideSearcher.setOnClickListener {
            binding.searcherTop.searcherContainer.toggleTop()
        }
    }

    private fun goToFilters() {
        val sortFragment = SortFragment()
        sortFragment.show(parentFragmentManager, "")
    }

    private fun setupAdapter() {
        binding.notesRecycler.apply {
            adapter = noteListAdapter
            layoutManager = LinearLayoutManager(requireContext())
            isNestedScrollingEnabled = true
            noteListAdapter.setOnItemClickListener(object : OnNoteClickListener {
                override fun onItemClick(note: Note, count: Int) {
                    launchDeleteConfirmation(note, count)
                }

                override fun onEditItemClick(id: Int, view: View, transitionName: String) {
                    activity.goToNoteEditorById(id, view, transitionName)
                }

                override fun onMultiSelection(notesSelected: HashMap<Int, Note>) {
                    launchMultiSelectionOptions(notesSelected)
                }

                override fun onEmptyNotes() {
                    showEmptyNotes()
                }
            })
        }
    }

    private fun launchMultiSelectionOptions(notesSelected: HashMap<Int, Note>) {
        with(binding.optionsMultiSelection) {
            val onBack = requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
                mainContainer.isVisible = false
                noteListAdapter.uncheckAllItems(notesSelected)
                this.remove()
            }
            mainContainer.isVisible = notesSelected.isNotEmpty()
            quantitySelected.text =
                getString(R.string.notes_quantity_selected_label, notesSelected.size.toString())

            btnBack.setOnClickListener {
                mainContainer.isVisible = false
                noteListAdapter.uncheckAllItems(notesSelected)
                onBack.remove()
            }
            btnMultiDelete.setOnClickListener {
                launchMultiDeleteConfirmation(ArrayList(notesSelected.values))
            }
        }
    }

    private fun launchDeleteConfirmation(note: Note, count: Int) {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(getString(R.string.notes_delete_preparation))
            .setMessage(getString(R.string.notes_delete_confirmation))
            .setNegativeButton(getString(R.string.notes_cancel)) { dialog, _ ->
                dialog.cancel()
            }
            .setPositiveButton(getString(R.string.notes_delete)) { dialog, _ ->
                if (count <= 1) showEmptyNotes()
                notesViewModel.deleteNote(note)
                noteListAdapter.refresh()
                showSnackBarToRevert(arrayListOf(note))
                dialog.dismiss()
            }
            .show()
    }

    private fun launchMultiDeleteConfirmation(notes: ArrayList<Note>) {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(getString(R.string.notes_delete_preparation))
            .setMessage(getAppropriateMessage(notes))
            .setNegativeButton(getString(R.string.notes_cancel)) { dialog, _ ->
                dialog.cancel()
            }
            .setPositiveButton(getString(R.string.notes_delete)) { dialog, _ ->
                notes.forEach { note ->
                    notesViewModel.deleteNote(note)
                }
                binding.optionsMultiSelection.mainContainer.isVisible = false
                noteListAdapter.refresh()
                showSnackBarToRevert(notes)
                dialog.dismiss()
            }
            .show()
    }

    private fun getAppropriateMessage(notes: ArrayList<Note>): String {
        return if (notes.size > 1)
            getString(R.string.notes_multi_delete_confirmation, notes.size.toString())
        else getString(R.string.notes_delete_confirmation)
    }

    private fun showSnackBarToRevert(notesRecovery: ArrayList<Note>) {
        Snackbar.make(
            binding.containerMain,
            if (notesRecovery.size > 1)
                getString(R.string.note_multi_eliminated)
            else getString(R.string.note_eliminated),
            Snackbar.LENGTH_LONG
        ).setAction(getString(R.string.note_recovery)) {
            notesRecovery.forEach { note ->
                notesViewModel.insertNote(note)
            }
            if (!binding.notesRecycler.isVisible) hideEmptyNotes()
            noteListAdapter.refresh()
            binding.optionsMultiSelection.mainContainer.isVisible = true
        }.show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        private const val VISIBLE = true
        private const val NOT_VISIBLE = false
    }
}