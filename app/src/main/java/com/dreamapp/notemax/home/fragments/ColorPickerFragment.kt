package com.dreamapp.notemax.home.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.dreamapp.notemax.R
import com.dreamapp.notemax.core.OnColorClickListener
import com.dreamapp.notemax.databinding.FragmentColorSelectionBinding
import com.dreamapp.notemax.domain.model.Color
import com.dreamapp.notemax.domain.viewmodel.NotesViewModel
import com.dreamapp.notemax.home.adapters.ColorSelectionAdapter
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import java.util.ArrayList

class ColorPickerFragment : BottomSheetDialogFragment() {

    private lateinit var colorsAdapter: ColorSelectionAdapter

    private var _binding: FragmentColorSelectionBinding? = null
    private val binding get() = _binding!!

    private val notesViewModel: NotesViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = initViewBinding(inflater, container)

    private fun initViewBinding(inflater: LayoutInflater, container: ViewGroup?): View {
        _binding = FragmentColorSelectionBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        colorsAdapter = ColorSelectionAdapter(getColorsAvailable())
        binding.rvColors.apply {
            adapter = colorsAdapter
            layoutManager = GridLayoutManager(requireContext(), 8)
            isNestedScrollingEnabled = true
        }
        colorsAdapter.setOnItemClickListener(object : OnColorClickListener {
            override fun onColorSelected(color: Color) {
                notesViewModel.colorSelected.value = color
                dismiss()
            }
        })
    }

    private fun getColorsAvailable(): ArrayList<Color> {
        return arrayListOf(
            Color(0, R.color.bright_sun),
            Color(1, R.color.maroon_flush1),
            Color(2, R.color.maroon_flush2),
            Color(3, R.color.maroon_flush3),
            Color(4, R.color.maroon_flush4)
        )
    }
}