package com.dreamapp.notemax.home.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.dreamapp.notemax.core.OnColorClickListener
import com.dreamapp.notemax.databinding.LayoutColorItemBinding
import com.dreamapp.notemax.domain.model.Color

class ColorSelectionAdapter(private val colors: ArrayList<Color>) :
    RecyclerView.Adapter<ColorSelectionAdapter.ColorViewHolder>() {

    private lateinit var clickListener: OnColorClickListener

    class ColorViewHolder(val binding: LayoutColorItemBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ColorViewHolder {
        return ColorViewHolder(
            LayoutColorItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                ATTACH_TO_PARENT
            )
        )
    }

    override fun getItemCount(): Int = colors.size

    override fun onBindViewHolder(holder: ColorViewHolder, position: Int) {
        with(holder.binding) {
            cvColorCard.setCardBackgroundColor(ContextCompat.getColor(root.context, colors[position].color))
            cvColorCard.setOnClickListener {
                clickListener.onColorSelected(colors[position])
            }
        }
    }

    fun setOnItemClickListener(listener: OnColorClickListener) {
        clickListener = listener
    }

    companion object {
        private const val ATTACH_TO_PARENT = false
    }
}
