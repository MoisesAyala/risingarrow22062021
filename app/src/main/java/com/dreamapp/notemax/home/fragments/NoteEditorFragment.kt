package com.dreamapp.notemax.home.fragments

import android.graphics.Typeface
import android.os.Bundle
import android.text.Spannable
import android.text.style.CharacterStyle
import android.text.style.StrikethroughSpan
import android.text.style.StyleSpan
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat.fromHtml
import androidx.core.text.HtmlCompat.toHtml
import androidx.core.text.HtmlCompat.FROM_HTML_MODE_LEGACY
import androidx.core.text.HtmlCompat.TO_HTML_PARAGRAPH_LINES_CONSECUTIVE
import androidx.core.text.toSpanned
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.dreamapp.notemax.R
import com.dreamapp.notemax.core.EditTextUndoRedo
import com.dreamapp.notemax.core.setStringPreference
import com.dreamapp.notemax.databinding.FragmentNoteEditorBinding
import com.dreamapp.notemax.domain.model.CheckList
import com.dreamapp.notemax.domain.model.Note
import com.dreamapp.notemax.domain.viewmodel.NotesViewModel
import com.dreamapp.notemax.home.HomeActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.transition.platform.MaterialContainerTransform
import java.text.SimpleDateFormat
import java.util.*

class NoteEditorFragment : Fragment() {

    private lateinit var activity: HomeActivity
    private var _binding: FragmentNoteEditorBinding? = null
    private val binding get() = _binding!!
    private var isNewNote = true
    private var idSelected = 0
    private var transitionName = ""

    lateinit var noteInUse: Note

    private var colorSelected = R.color.bright_sun
    private var colorPickerWasUsed = false
    private lateinit var mTextViewUndoRedo: EditTextUndoRedo

    private val notesViewModel: NotesViewModel by activityViewModels()

    companion object {
        fun editNoteInstance(id: Int, transitionName: String) = NoteEditorFragment().apply {
            arguments = Bundle().apply {
                this.putInt("id", id)
                this.putBoolean("isNewNote", false)
                this.putString("transitionName", transitionName)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = context as HomeActivity
        arguments?.let {
            idSelected = it.getInt("id", 0)
            isNewNote = it.getBoolean("isNewNote", true)
            transitionName = it.getString("transitionName", "")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View = initViewBinding(inflater)

    private fun initViewBinding(inflater: LayoutInflater): View {
        _binding = FragmentNoteEditorBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mTextViewUndoRedo = EditTextUndoRedo(binding.contentEditText)
        if (isNewNote) {
            binding.clEditor.transitionName = "shared_element_container"
        } else {
            binding.clEditor.transitionName = transitionName
        }
        sharedElementEnterTransition = MaterialContainerTransform()

        initClickListeners()
        requireActivity().setStringPreference("KEY_TEST", "WasInOnResumePapi")
        notesViewModel.colorSelected.observe(viewLifecycleOwner) { colorResult ->
            if (colorPickerWasUsed) {
                colorSelected = colorResult.color
            }
            setBackgroundColor(colorSelected)
        }

        when {
            isNewNote -> setBackgroundColor(colorSelected)
            else -> {
                setNoteDataSelected()
                callNoteSelected()
            }
        }
    }

    private fun setBackgroundColor(color: Int) {
        binding.circle.backgroundTintList =
            ContextCompat.getColorStateList(requireContext(), color)
        binding.clEditor.setBackgroundColor(
            ContextCompat.getColor(
                requireContext(),
                color
            )
        )
    }

    private fun goToColorSelection() {
        val colorFragment = ColorPickerFragment()
        colorPickerWasUsed = true
        colorFragment.show(parentFragmentManager, "")
    }

    private fun callNoteSelected() {
        notesViewModel.getNoteById(idSelected)
    }

    private fun setNoteDataSelected() {
        notesViewModel.getNoteById.observe(viewLifecycleOwner) { note ->
            if (note.title.isNotEmpty()) {
                noteInUse = note
                binding.toolsContainer.btnDelete.isEnabled = true

                binding.titleEditText.setText(fromHtml(note.title, FROM_HTML_MODE_LEGACY))
                binding.contentEditText.setText(fromHtml(note.content, FROM_HTML_MODE_LEGACY))
                colorSelected = note.color
                setBackgroundColor(note.color)
            }
        }
    }

    private fun Date.toString(format: String, locale: Locale = Locale.getDefault()): String {
        val formatter = SimpleDateFormat(format, locale)
        return formatter.format(this)
    }

    private fun getCurrentDateTime(): Date {
        return Calendar.getInstance().time
    }

    private fun initClickListeners() {
        with(binding) {
            saveNote.setOnClickListener {
                if (noteIsCompleted()) {
                    if (!isNewNote)
                        notesViewModel.updateNote(
                            Note(
                                id = idSelected,
                                title = getTitle(),
                                content = getContent(),
                                date = getDate(),
                                color = colorSelected,
                                font = 0,
                                archived = noteInUse.archived,
                                checkList = arrayListOf()
                            )
                        )
                    else notesViewModel.insertNote(
                        Note(
                            title = getTitle(),
                            content = getContent(),
                            date = getDate(),
                            color = colorSelected,
                            font = 0,
                            archived = false,
                            checkList = arrayListOf(CheckList("Test", true))
                        )
                    )
                    activity.goToNotesListBack()
                }
            }

            btnBack.setOnClickListener {
                parentFragmentManager.popBackStack()
            }
            toolsContainer.btnPalette.setOnClickListener {
                goToColorSelection()
            }
            toolsContainer.btnDelete.setOnClickListener {
                launchDeleteConfirmation()
            }
            toolsContainer.bold.setOnClickListener {
                setTextStyle(TextStyle.BOLD)
            }
            toolsContainer.italic.setOnClickListener {
                setTextStyle(TextStyle.ITALIC)
            }
            toolsContainer.underlined.setOnClickListener {
                setTextStyle(TextStyle.UNDERLINED)
            }
            toolsContainer.strikeThrough.setOnClickListener {
                setTextStyle(TextStyle.STRIKETHROUGH)
            }
            toolsContainer.formatSize.setOnClickListener {
            }
            binding.btnUndo.setOnClickListener {
                if (mTextViewUndoRedo.canUndo)
                    mTextViewUndoRedo.undo()
            }
            binding.btnRedo.setOnClickListener {
                if (mTextViewUndoRedo.canRedo)
                    mTextViewUndoRedo.redo()
            }
        }
    }

    private fun getDate(): String {
        return getCurrentDateTime().toString("dd/MM/yyyy HH:mm:ss")
    }

    private fun getTitle(): String {
        return binding.titleEditText.text.toString()
    }

    private fun getContent(): String {
        return toHtml(
            binding.contentEditText.text?.toSpanned() ?: "".toSpanned(),
            TO_HTML_PARAGRAPH_LINES_CONSECUTIVE
        )
    }

    private fun setTextStyle(style: TextStyle) {
        when (style) {
            TextStyle.BOLD -> {
                setTextStyle(StyleSpan(Typeface.BOLD))
            }
            TextStyle.ITALIC -> {
                setTextStyle(StyleSpan(Typeface.ITALIC))
            }
            TextStyle.UNDERLINED -> {
                setCharacterStyle(UnderlineSpan())
            }
            TextStyle.STRIKETHROUGH -> {
                setCharacterStyle(StrikethroughSpan())
            }
        }
    }

    private fun launchDeleteConfirmation() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(getString(R.string.notes_delete_preparation))
            .setMessage(getString(R.string.notes_delete_confirmation))
            .setNegativeButton(getString(R.string.notes_cancel)) { dialog, _ ->
                dialog.cancel()
            }
            .setPositiveButton(getString(R.string.notes_delete)) { dialog, _ ->
                notesViewModel.deleteNote(noteInUse)
                activity.goToNotesListBack()
                dialog.dismiss()
            }
            .show()
    }

    private fun setTextStyle(style: StyleSpan) {
        with(binding.contentEditText) {
            if (selectionStart != selectionEnd && selectionEnd != -1) {
                if (!existStyleSpan(style)) {
                    text?.setSpan(
                        style,
                        selectionStart,
                        selectionEnd,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                    )
                }
            }
        }
    }

    private fun setCharacterStyle(style: CharacterStyle) {
        with(binding.contentEditText) {
            if (selectionStart != selectionEnd && selectionEnd != -1) {
                if (!existCharacterStyle(style)) {
                    text?.setSpan(
                        style,
                        selectionStart,
                        selectionEnd,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                    )
                }
            }
        }
    }

    private fun EditText.existStyleSpan(styleToSearch: StyleSpan): Boolean {
        text.getSpans(selectionStart, selectionEnd, StyleSpan::class.java).map {
            if (it.style == styleToSearch.style) {
                text.removeSpan(it)
                return true
            }
        }
        return false
    }

    private fun EditText.existCharacterStyle(characterStyle: CharacterStyle): Boolean {
        text.getSpans(selectionStart, selectionEnd, characterStyle::class.java).map {
            text.removeSpan(it)
            return true
        }
        return false
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun noteIsCompleted(): Boolean {
        return binding.contentEditText.text!!.isNotBlank() && binding.titleEditText.text!!.isNotBlank()
    }

    enum class TextStyle {
        BOLD, ITALIC, UNDERLINED, STRIKETHROUGH
    }
}