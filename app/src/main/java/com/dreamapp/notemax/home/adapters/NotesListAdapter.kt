package com.dreamapp.notemax.home.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.MenuRes
import androidx.appcompat.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat.FROM_HTML_MODE_LEGACY
import androidx.core.text.HtmlCompat.fromHtml
import androidx.core.view.isVisible
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.dreamapp.notemax.R
import com.dreamapp.notemax.core.OnNoteClickListener
import com.dreamapp.notemax.databinding.LayoutNoteItemBinding
import com.dreamapp.notemax.domain.model.Note

class NotesListAdapter(private val context: Context) :
    PagingDataAdapter<Note, NotesListAdapter.NotesListViewHolder>(NotesComparator) {

    private lateinit var clickListener: OnNoteClickListener
    private lateinit var editNoteListener: OnNoteClickListener
    private var notesSelected: HashMap<Int, Note> = hashMapOf()

    inner class NotesListViewHolder(private val binding: LayoutNoteItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindPeople(item: Note) = with(binding) {
            containerCard.isChecked = notesSelected.containsValue(item)
            options.isVisible = !containerCard.isChecked
            containerCard.transitionName = "shared_container${item.title}"
            val title = item.title.replace("<br>", " ").replace("<p>", "")
            val content = item.content.replace("<br>", " ").replace("<p>", "")
            noteTitle.text = fromHtml(title, FROM_HTML_MODE_LEGACY).toString()
            noteContent.text = fromHtml(content, FROM_HTML_MODE_LEGACY).toString()
            date.text = item.date
            containerCard.setCardBackgroundColor(ContextCompat.getColor(root.context, item.color))
            options.setOnClickListener {
                showMenu(it, R.menu.note_item_options, item, itemCount)
            }
            containerCard.setOnLongClickListener {
                /*
                checkedIcon.isVisible = true
                options.isVisible = false
                containerCard.requestFocus()*/
                containerCard.isChecked = !containerCard.isChecked
                options.isVisible = !containerCard.isChecked
                if (containerCard.isChecked)
                    notesSelected[layoutPosition] = item
                else notesSelected.remove(layoutPosition)
                editNoteListener.onMultiSelection(notesSelected)
                true
            }
            containerCard.setOnClickListener {
                when {
                    containerCard.isChecked -> {
                        containerCard.isChecked = false
                        options.isVisible = !containerCard.isChecked
                        notesSelected.remove(layoutPosition)
                        editNoteListener.onMultiSelection(notesSelected)
                    }
                    notesSelected.isNotEmpty() -> {
                        containerCard.isChecked = true
                        options.isVisible = !containerCard.isChecked
                        notesSelected[layoutPosition] = item
                        editNoteListener.onMultiSelection(notesSelected)
                    }
                    else -> {
                        editNoteListener.onEditItemClick(
                            item.id,
                            it,
                            containerCard.transitionName.toString()
                        )
                    }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotesListViewHolder {
        return NotesListViewHolder(
            LayoutNoteItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    object NotesComparator : DiffUtil.ItemCallback<Note>() {
        override fun areItemsTheSame(oldItem: Note, newItem: Note): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Note, newItem: Note): Boolean {
            return oldItem == newItem
        }
    }

    override fun onBindViewHolder(holder: NotesListViewHolder, position: Int) {
        val item = getItem(position)
        item?.let { holder.bindPeople(it) }
    }

    private fun showMenu(v: View, @MenuRes menuRes: Int, item: Note, itemCount: Int) {
        val popup = PopupMenu(context, v)
        popup.menuInflater.inflate(menuRes, popup.menu)

        popup.setOnMenuItemClickListener { menuItem: MenuItem ->
            clickListener.onItemClick(item, itemCount)
            //notifyItemRemoved(layoutPosition)
            true
        }
        popup.setOnDismissListener {
            // Respond to popup being dismissed.
        }
        // Show the popup menu.
        popup.show()
    }

    fun setOnItemClickListener(listener: OnNoteClickListener) {
        clickListener = listener
        editNoteListener = listener
    }

    fun uncheckAllItems(notes: HashMap<Int, Note>) {
        this.notesSelected = hashMapOf()
        notes.keys.map {
            notifyItemChanged(it)
        }
    }
}