package com.dreamapp.notemax.home.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.dreamapp.notemax.databinding.FragmentNotesArchivedBinding
import com.dreamapp.notemax.domain.viewmodel.NotesViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class NotesArchivedFragment : Fragment() {

    private var _binding: FragmentNotesArchivedBinding? = null
    private val binding get() = _binding!!

    private val notesViewModel: NotesViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View = initViewBinding(inflater)

    private fun initViewBinding(inflater: LayoutInflater): View {
        _binding = FragmentNotesArchivedBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launch {
            getNotesArchived()
        }
    }

    private suspend fun getNotesArchived() {
        notesViewModel.getNotesArchived().collect { notesArchived ->
            //binding.notesArchived.initialize(notesArchived)
        }
    }
}