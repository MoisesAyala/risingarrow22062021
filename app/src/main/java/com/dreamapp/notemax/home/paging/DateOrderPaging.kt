package com.dreamapp.notemax.home.paging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.dreamapp.notemax.domain.model.Note
import com.dreamapp.notemax.domain.repository.NotesRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DateOrderPaging(private val repository: NotesRepository) :
    PagingSource<Int, Note>() {
    override fun getRefreshKey(state: PagingState<Int, Note>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Note> {
        return withContext(Dispatchers.IO) {
            val response = repository.getNotesByDate()

            LoadResult.Page(
                data = response,
                prevKey = null,
                nextKey = null
            )
        }
    }
}