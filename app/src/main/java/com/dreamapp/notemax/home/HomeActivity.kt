package com.dreamapp.notemax.home

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.window.OnBackInvokedDispatcher
import androidx.fragment.app.FragmentTransaction
import com.dreamapp.notemax.R
import com.dreamapp.notemax.home.fragments.NoteEditorFragment
import com.dreamapp.notemax.home.fragments.NotesFragment
import com.google.android.material.color.DynamicColors
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        supportActionBar?.hide()
        goToNotesList()
    }

    private fun goToNotesList() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, NotesFragment())
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()
    }

    fun goToNotesListBack() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, NotesFragment())
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
            .commit()
    }

    fun goToNoteEditorById(id: Int, view: View, transitionName: String) {
        supportFragmentManager.beginTransaction()
            .setReorderingAllowed(true)
            .replace(R.id.container, NoteEditorFragment.editNoteInstance(id, transitionName))
            .addSharedElement(view, transitionName)
            //.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .addToBackStack(null)
            .commit()
    }

    fun goToNoteEditor(view: View) {
        supportFragmentManager.beginTransaction()
            .add(R.id.container, NoteEditorFragment())
            .addSharedElement(view, "shared_element_container")
            //.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .addToBackStack(null)
            .commit()
    }
}