package com.dreamapp.notemax.home.fragments

import android.content.ContentValues.TAG
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.dreamapp.notemax.core.NotesBackUpAgent
import com.dreamapp.notemax.databinding.FragmentLoginBinding
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.drive.Drive
import com.google.android.gms.drive.Drive.SCOPE_APPFOLDER
import com.google.android.gms.tasks.Task
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.coroutines.launch
import okio.Okio
import java.io.File


class LoginFragment : BottomSheetDialogFragment() {

    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private val RC_SIGN_IN = 1
    private val RC_REQUEST_PERMISSION_SUCCESS_CONTINUE_FILE_CREATION = 2

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = initViewBinding(inflater, container)

    private fun initViewBinding(inflater: LayoutInflater, container: ViewGroup?): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()

        val mGoogleSignInClient = GoogleSignIn.getClient(requireActivity(), gso)

        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.
        val acct = GoogleSignIn.getLastSignedInAccount(requireActivity())
        if (acct != null) {
            val personName = acct.givenName
            val personGivenName = acct.givenName
            val personFamilyName = acct.familyName
            val personEmail = acct.email
            val personId = acct.id
            val personPhoto: Uri? = acct.photoUrl
        }

        if (!GoogleSignIn.hasPermissions(
                GoogleSignIn.getLastSignedInAccount(requireActivity()),
                SCOPE_APPFOLDER
            )
        ) {
            GoogleSignIn.requestPermissions(
                requireActivity(),
                RC_REQUEST_PERMISSION_SUCCESS_CONTINUE_FILE_CREATION,
                GoogleSignIn.getLastSignedInAccount(requireActivity()),
                SCOPE_APPFOLDER
            )
        } else {
            //saveToDriveAppFolder()
            Toast.makeText(requireContext(), "PERMISSION GRANTED DRIVE", Toast.LENGTH_SHORT).show()
        }



        binding.btnLogin.setOnClickListener {
            val signInIntent: Intent = mGoogleSignInClient.signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            RC_SIGN_IN -> {
                val task: Task<GoogleSignInAccount> =
                    GoogleSignIn.getSignedInAccountFromIntent(data)
                handleSignInResult(task)
            }
            RC_REQUEST_PERMISSION_SUCCESS_CONTINUE_FILE_CREATION -> {
                Toast.makeText(requireContext(), "PERMISSION GRANTED DRIVE2", Toast.LENGTH_SHORT).show()
            }
        }

    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)

            // Signed in successfully, show authenticated UI.
            println(account.toString())
            val backup = NotesBackUpAgent()
            backup.context = requireActivity()
            backup.requestBackup()
            parentFragmentManager.popBackStack()
        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.statusCode)
            e.printStackTrace()
        }
    }

}