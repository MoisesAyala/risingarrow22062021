package com.dreamapp.notemax.core

import androidx.room.TypeConverter
import com.dreamapp.notemax.domain.model.CheckList
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class Converters {

    @TypeConverter
    fun restoreList(listOfString: String?): ArrayList<CheckList?>? {
        return Gson().fromJson(listOfString, object : TypeToken<ArrayList<CheckList?>?>() {}.type)
    }

    @TypeConverter
    fun saveList(listOfString: ArrayList<CheckList>): String? {
        return Gson().toJson(listOfString)
    }
}