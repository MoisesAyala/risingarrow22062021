package com.dreamapp.notemax.core

import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isGone
import androidx.transition.Slide
import androidx.transition.Transition
import androidx.transition.TransitionManager


fun ViewGroup.toggleTop(duration: Int = 300) {
    val transition: Transition = Slide(Gravity.TOP)
    transition.duration = duration.toLong()
    TransitionManager.beginDelayedTransition(this, transition)
    this.visibility = if (this.isGone) View.VISIBLE else View.GONE
}