package com.dreamapp.notemax.core

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences

fun Activity.setStringPreference(key: String, value: String) {
    val sharedPref = this.getSharedPreferences("MAIN_PREFERENCES", Context.MODE_PRIVATE) ?: return
    with(sharedPref.edit()) {
        putString(key, value)
        apply()
    }
}

fun Activity.getStringPreference(key: String, default: String): String {
    val sharedPref =
        this.getSharedPreferences("MAIN_PREFERENCES", Context.MODE_PRIVATE) ?: return default
    with(sharedPref) {
        return getString(key, default)!!
    }
}