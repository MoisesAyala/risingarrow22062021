package com.dreamapp.notemax.core

import android.content.Context
import android.content.res.Configuration

fun Context.isDarkThemeOn(): Boolean {
    return when (resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
        Configuration.UI_MODE_NIGHT_NO -> false
        Configuration.UI_MODE_NIGHT_YES -> true
        else -> false
    }
}