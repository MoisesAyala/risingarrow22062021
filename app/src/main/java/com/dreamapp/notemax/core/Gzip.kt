package com.dreamapp.notemax.core

import android.os.Build
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.*
import java.util.zip.GZIPOutputStream

fun compress(text: String): String {
    val baos = ByteArrayOutputStream()

    baos.write(byteArrayOf(0x05, 0, 0, 0), 0, 4)
    val gzos = GZIPOutputStream(baos)
    gzos.write(text.toByteArray())
    gzos.close()
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        Base64.getEncoder().encodeToString(baos.toByteArray())
    } else return ""
}