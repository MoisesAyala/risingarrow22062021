package com.dreamapp.notemax.core

import android.view.View
import com.dreamapp.notemax.domain.model.Color
import com.dreamapp.notemax.domain.model.Note

interface OnNoteClickListener {
    fun onItemClick(note: Note, count: Int)
    fun onEditItemClick(id: Int, view: View, transitionName: String)
    fun onMultiSelection(notesSelected: HashMap<Int, Note>)
    fun onEmptyNotes()
}

interface OnColorClickListener {
    fun onColorSelected(color: Color)
}