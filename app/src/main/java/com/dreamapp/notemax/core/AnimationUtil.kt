package com.dreamapp.notemax.core

import android.view.animation.Animation
import android.view.View
import javax.inject.Singleton

@Singleton
fun View.animationStart(animation: Animation, onEnd: () -> Unit) {
    animation.setAnimationListener(object : Animation.AnimationListener {
        override fun onAnimationStart(p0: Animation?) {
        }

        override fun onAnimationEnd(p0: Animation?) {
            onEnd()
        }

        override fun onAnimationRepeat(p0: Animation?) {

        }
    })
    this.startAnimation(animation)
}