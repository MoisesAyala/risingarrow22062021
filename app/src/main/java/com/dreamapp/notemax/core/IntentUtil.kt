package com.dreamapp.notemax.core

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.dreamapp.notemax.home.HomeActivity

object IntentUtil {

    fun intentToHome(context: Context, extras: Bundle) {
        val intent = Intent(context, HomeActivity::class.java)
        intent.putExtras(extras)
        context.startActivity(intent)
    }

}