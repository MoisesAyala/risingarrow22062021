package com.dreamapp.notemax.core

import android.app.backup.*
import android.content.Context
import android.os.ParcelFileDescriptor
import android.widget.Toast
import com.dreamapp.notemax.domain.repository.NotesRepository
import javax.inject.Inject


class NotesBackUpAgent : BackupAgentHelper() {
    // The name of the file
    lateinit var context: Context
    val MAIN_PREFERENCES = "MAIN_PREFERENCES"

    // A key to uniquely identify the set of backup data
    val FILES_BACKUP_KEY = "myfiles"
    override fun onCreate() {
        // Allocate a helper and add it to the backup agent
        // Allocate a helper and add it to the backup agent
        SharedPreferencesBackupHelper(context, MAIN_PREFERENCES).also {
            addHelper(FILES_BACKUP_KEY, it)
        }
    }

    fun requestBackup() {
        val bm = BackupManager(context)
        bm.dataChanged()
        Toast.makeText(context, "Se solicitó sync", Toast.LENGTH_SHORT).show()
    }

    override fun onRestore(
        data: BackupDataInput?,
        appVersionCode: Int,
        newState: ParcelFileDescriptor?
    ) {
        super.onRestore(data, appVersionCode, newState)
        Toast.makeText(context, "Se hizo restore", Toast.LENGTH_SHORT).show()
    }

    override fun onBackup(
        oldState: ParcelFileDescriptor?,
        data: BackupDataOutput?,
        newState: ParcelFileDescriptor?
    ) {
        super.onBackup(oldState, data, newState)
        Toast.makeText(context, "Se hizo backup", Toast.LENGTH_SHORT).show()
    }

    override fun onQuotaExceeded(backupDataBytes: Long, quotaBytes: Long) {
        super.onQuotaExceeded(backupDataBytes, quotaBytes)
        Toast.makeText(context, "Se excedió el almacenamiento", Toast.LENGTH_SHORT).show()
    }

    override fun onRestoreFinished() {
        super.onRestoreFinished()
    }

}