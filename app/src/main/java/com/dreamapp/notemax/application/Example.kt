package com.dreamapp.notemax.application

/*
•	The items shown on the map for a given area are hotels, airports, and tourist point of interest (POI).
•	hotels and  POI have a rating out of 5 stars (1, 1.5, 2, 2.5, ... 5.0) and a location.
•	Point of Interest each has a fixed price: £10, £25, £50 or £100.
.	Airport each has a location
•	An itinerary is a list of the above 3 kind of items and there will be no duplicated item


class itinerary: Service  {
Airport - Location
Hotels - rating, Location
POI - rating, Location, fixedPrice
}


1.	Design: Explain the objects that you would use to represent the above system.
2.	Design: Assumed that a tourist has an itinerary to visit. As the tourist visits each location in the itinerary, create a function to get the total distance traveled until that location.
*/




//---------------------------------
/*
data class (var itinerary: Service?)
class Itinerary: Service {
    //implement members


}

lateinit var repository: Service
repository.
*/
interface Service {

    fun printPrices()
}

interface Airport: Service {
    override fun printPrices() {
        println("This are airport prices")
    }
}

interface Hotel: Service {
    override fun printPrices() {
        println("This are Hotel prices")
    }
}