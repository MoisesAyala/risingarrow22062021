package com.dreamapp.notemax.application

import android.app.Activity
import android.app.Application
import android.os.Bundle
import android.widget.Toast

class ActivityLifecycleListener(private val application: Application) :
    Application.ActivityLifecycleCallbacks {

    override fun onActivityPaused(p0: Activity) {
        Toast.makeText(application.applicationContext, "onActivityPaused $p0", Toast.LENGTH_SHORT)
            .show()
    }

    override fun onActivityStarted(p0: Activity) {
        Toast.makeText(application.applicationContext, "onActivityStarted $p0", Toast.LENGTH_SHORT)
            .show()
    }

    override fun onActivityDestroyed(p0: Activity) {
        Toast.makeText(application.applicationContext, "onActivityDestroyed $p0", Toast.LENGTH_SHORT)
            .show()
    }

    override fun onActivitySaveInstanceState(p0: Activity, p1: Bundle) {
        Toast.makeText(application.applicationContext, "onActivitySaveInstanceState $p0", Toast.LENGTH_SHORT)
            .show()
    }

    override fun onActivityStopped(p0: Activity) {
        Toast.makeText(application.applicationContext, "onActivityStopped $p0", Toast.LENGTH_SHORT)
            .show()
    }

    override fun onActivityCreated(p0: Activity, p1: Bundle?) {
        Toast.makeText(application.applicationContext, "onActivityCreated $p0", Toast.LENGTH_SHORT)
            .show()
    }

    override fun onActivityResumed(p0: Activity) {
        Toast.makeText(application.applicationContext, "onActivityResumed $p0", Toast.LENGTH_SHORT)
            .show()
    }

    companion object : Application.ActivityLifecycleCallbacks {
        private const val TAG = "LifecycleCallbacks"
        override fun onActivityCreated(p0: Activity, p1: Bundle?) {
            println("OnActivityCreated")
        }

        override fun onActivityStarted(p0: Activity) {
            println("OnActivityStarted")
        }

        override fun onActivityResumed(p0: Activity) {
            //Empty
        }

        override fun onActivityPaused(p0: Activity) {
            println("OnActivityPaused")
        }

        override fun onActivityStopped(p0: Activity) {
            println("OnActivityStopped$p0")
        }

        override fun onActivitySaveInstanceState(p0: Activity, p1: Bundle) {
            //Empty
        }

        override fun onActivityDestroyed(p0: Activity) {
            println("OnActivityDestroyed$p0")
        }
    }
}