package com.dreamapp.notemax.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.dreamapp.notemax.core.Converters
import com.dreamapp.notemax.domain.model.Note

@Database(
    entities = [Note::class],
    version = 3,
    exportSchema = false,
)
@TypeConverters(Converters::class)
abstract class NotesDB : RoomDatabase() {

    abstract fun noteDao(): NoteDao

    companion object {
        @Volatile
        private var INSTANCE: NotesDB? = null

        fun getDatabase(context: Context): NotesDB {
            val tempInstance = INSTANCE
            if (tempInstance != null) return tempInstance

            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    NotesDB::class.java,
                    "note_table"
                )
                    .addMigrations(MIGRATION_2_3())
                    .build()
                INSTANCE = instance
                return instance
            }
        }
    }

    class MIGRATION_2_3 : Migration(2, 3) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("ALTER TABLE note_table ADD test INTEGER NOT NULL DEFAULT 0")
        }
    }
}