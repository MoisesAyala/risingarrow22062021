package com.dreamapp.notemax.data

import androidx.room.*
import com.dreamapp.notemax.domain.model.Note

@Dao
interface NoteDao {
    @Query("SELECT * FROM note_table WHERE archived = 0 ORDER BY date DESC")
    suspend fun getNotes(): List<Note>

    @Query("SELECT * FROM note_table ORDER BY date DESC")
    suspend fun getNotesByDate(): List<Note>

    @Query("SELECT * FROM note_table ORDER BY title ASC")
    suspend fun getNotesByTitle(): List<Note>

    @Query("SELECT * FROM note_table ORDER BY color ASC")
    suspend fun getNotesByColor(): List<Note>

    @Query("SELECT * FROM note_table WHERE id = :id")
    suspend fun getNoteById(id: Int): Note

    @Query("SELECT * FROM note_table WHERE title LIKE :search OR content LIKE :search OR date LIKE :search")
    suspend fun getSearchResults(search: String): List<Note>

    @Query("SELECT * FROM note_table WHERE archived = 1")
    suspend fun getNotesArchived(): List<Note>

    @Update
    suspend fun updateNote(note: Note)

    @Update
    suspend fun updateNotes(notes: List<Note>)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertNote(note: Note)

    @Delete
    suspend fun deleteNote(note: Note)
}