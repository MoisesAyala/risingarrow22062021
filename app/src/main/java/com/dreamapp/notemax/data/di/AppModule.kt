package com.dreamapp.notemax.data.di

import android.app.Application
import com.dreamapp.notemax.data.NoteDao
import com.dreamapp.notemax.data.NotesDB
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun getAppDB(context: Application): NotesDB {
        return NotesDB.getDatabase(context)
    }

    @Singleton
    @Provides
    fun getDao(notesDB: NotesDB): NoteDao {
        return notesDB.noteDao()
    }
}