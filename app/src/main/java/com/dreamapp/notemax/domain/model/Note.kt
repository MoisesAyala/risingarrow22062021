package com.dreamapp.notemax.domain.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "note_table")
data class Note(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    var title: String,
    var content: String,
    var date: String,
    var color: Int,
    var font: Int,
    var archived: Boolean,
    var checkList: ArrayList<CheckList>
)