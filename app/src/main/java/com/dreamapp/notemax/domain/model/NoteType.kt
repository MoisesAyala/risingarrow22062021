package com.dreamapp.notemax.domain.model

sealed interface NoteType {
    object Note : NoteType
    object CheckList : NoteType
}