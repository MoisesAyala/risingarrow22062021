package com.dreamapp.notemax.domain.model

data class Color(
    val id: Int,
    val color: Int
)
