package com.dreamapp.notemax.domain.repository

import com.dreamapp.notemax.data.NoteDao
import com.dreamapp.notemax.domain.model.Note
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject


class NotesRepository @Inject constructor(private val notesDao: NoteDao) {

    suspend fun getNotes(): Flow<List<Note>> = flow {
        emit(notesDao.getNotes())
    }

    suspend fun getSearchResults(search: String): List<Note> {
        return notesDao.getSearchResults(search)
    }

    suspend fun getNotesByTitle(): List<Note> {
        return notesDao.getNotesByTitle()
    }

    suspend fun getNotesByDate(): List<Note> {
        return notesDao.getNotesByDate()
    }

    suspend fun getNotesByColor(): List<Note> {
        return notesDao.getNotesByColor()
    }

    suspend fun getNotesArchived(): Flow<List<Note>> = flow {
        emit(notesDao.getNotesArchived())
    }

    suspend fun insertNote(note: Note) {
        notesDao.insertNote(note)
    }

    suspend fun deleteNote(note: Note) {
        notesDao.deleteNote(note)
    }

    suspend fun updateNote(note: Note) {
        notesDao.updateNote(note)
    }

    suspend fun updateAllNotes(notes: List<Note>) {
        notesDao.updateNotes(notes)
    }

    suspend fun getNoteById(id: Int): Note {
        return notesDao.getNoteById(id)
    }
}