package com.dreamapp.notemax.domain.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import com.dreamapp.notemax.domain.model.Color
import com.dreamapp.notemax.domain.model.Note
import com.dreamapp.notemax.domain.repository.NotesRepository
import com.dreamapp.notemax.home.paging.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NotesViewModel @Inject constructor(
    private val repository: NotesRepository
) : ViewModel() {

    var getNoteById = MutableLiveData<Note>()
    var orderSelected = MutableLiveData<Int>()
    var colorSelected = MutableLiveData<Color>()
    var currentSearch = ""

    val notesPaging =
        Pager(config = PagingConfig(pageSize = 6), pagingSourceFactory = {
            NoteListPaging(repository)
        }).flow.cachedIn(viewModelScope)

    val notesSearchPaging =
        Pager(config = PagingConfig(pageSize = 6), pagingSourceFactory = {
            NoteSearchPaging(repository, currentSearch)
        }).flow.cachedIn(viewModelScope)

    val titleOrderPaging =
        Pager(config = PagingConfig(pageSize = 6), pagingSourceFactory = {
            TitleOrderPaging(repository)
        }).flow.cachedIn(viewModelScope)

    val dateOrderPaging =
        Pager(config = PagingConfig(pageSize = 6), pagingSourceFactory = {
            DateOrderPaging(repository)
        }).flow.cachedIn(viewModelScope)

    val colorOrderPaging =
        Pager(config = PagingConfig(pageSize = 6), pagingSourceFactory = {
            ColorOrderPaging(repository)
        }).flow.cachedIn(viewModelScope)

    suspend fun getNotes(): Flow<List<Note>> {
        return repository.getNotes()
    }

    fun insertNote(note: Note) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.insertNote(note)
        }
    }

    fun deleteNote(note: Note) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteNote(note)
        }
    }

    fun updateNote(note: Note) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.updateNote(note)
        }
    }

    fun updateAllNotes(notes: List<Note>) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.updateAllNotes(notes)
        }
    }

    suspend fun getNotesArchived(): Flow<List<Note>> {
        return repository.getNotesArchived()
    }

    fun getNoteById(id: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            getNoteById.postValue(repository.getNoteById(id))
        }
    }

}