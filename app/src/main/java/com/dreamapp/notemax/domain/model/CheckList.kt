package com.dreamapp.notemax.domain.model

data class CheckList(
    var name: String,
    var checked: Boolean
)
